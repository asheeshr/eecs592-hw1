#!/usr/bin/python

import sys
import getopt
from collections import deque
import copy

'''
Constants
'''
TILE_WALL = 'X'
TILE_OPEN = ' '
TILE_START = 'S'
TILE_GOAL = 'G'
TILE_SWAMP = '*'
TILE_TOWER = 'O'
'''
'''


def process_arguments(argv):
    input_file = ''
    try:
        opts, args = getopt.getopt(argv, "hi:", ["ifile="])
    except getopt.GetoptError:
        print('-i <inputfile>')
        sys.exit(2)
    for opt, arg in opts:
        if opt == '-h':
            print('-i <inputfile> -o <outputfile>')
            sys.exit()
        elif opt in ("-i", "--ifile"):
            input_file = arg
    print('Input file is ', input_file)
    return input_file


def generate_map(input_file):
    input_text = open(input_file, 'r')
    # input_map = input_text.readline()
    [rows, columns] = [int(x) for x in input_text.readline().split()]
    print("Map size is ", rows, columns)
    input_map = [[0 for x in range(columns)] for y in range(rows)]
    for i in range(rows):
        line = input_text.readline()
        input_map[i] = [c for c in line if c != '\n']

    return input_map


def generate_important_states(input_map):
    start_state = []
    goal_state = []

    for index, row in enumerate(input_map):
        if TILE_START in row:
            start_state = [index, row.index(TILE_START)]
        if TILE_GOAL in row:
            goal_state = [index, row.index(TILE_GOAL)]

    return start_state, goal_state


def cost(tile):
    '''
    Return cost of stepping OUT OF tile.
    '''
    if tile == TILE_START:
        return 1
    if tile == TILE_GOAL or tile == TILE_OPEN:
        return 1
    if tile == TILE_SWAMP:
        return 3


def direction(src, dst):
    if dst[0] < src[0] and dst[1] == src[1]:
        return 'N'
    if dst[1] < src[1] and dst[0] == src[0]:
        return 'W'
    if dst[1] > src[1] and dst[0] == src[0]:
        return 'E'
    if dst[0] > src[0] and dst[1] == src[1]:
        return 'S'


class search_node_t:
    def __init__(self, position, parent, tile, dmg, estimate):
        self.path = []
        self.parent = parent
        self.position = tuple(position)
        self.tile = tile
        self.cost = 0
        self.health = 5
        if self.parent:
            self.cost += self.parent.cost + cost(self.parent.tile)
            self.health = self.parent.health - dmg
        self.estimate = estimate
        self.moves = 0


class search_tree_t:
    def __init__(self, start_state, goal_state, input_map):
        self.explored_list = {}
        self.frontier = {}
        self.frontier_queue = deque()
        self.start_state = tuple(start_state)
        self.goal_state = tuple(goal_state)
        self.map = input_map
        self.search_method = {
            'dfs': self.dfs,
            'bfs': self.bfs,
            'ucs': self.ucs
        }
        self.current_method = ''
        self.heuristic = ''
        self.nodes_examined = 0

    def init_search(self):
        self.explored_list = {}
        self.frontier = {}
        self.frontier[self.start_state] = \
            search_node_t(self.start_state, tuple(),
                          self.map[self.start_state[0]][self.start_state[1]],
                          self.damage(self.start_state),
                          self.heuristic_estimate(self.start_state))
        self.frontier_queue = deque()
        self.frontier_queue.append(self.start_state)
        self.nodes_examined = 0
        print("Start state ", self.start_state)
        print("Goal state ", self.goal_state)

    def is_goal(self, index):
        return index == self.goal_state

    def in_explored_list(self, index):
        return index in self.explored_list.keys()

    def in_frontier(self, index):
        return index in self.frontier_queue

    def damage(self, position):
        '''
        Returns positive damage. Should be subtracted from health.
        '''
        x = position[0]
        y = position[1]
        north = (x - 1, y)
        west = (x, y - 1)
        east = (x, y + 1)
        south = (x + 1, y)
        dmg = 0
        if self.map[north[0]][north[1]] == TILE_TOWER:
            dmg += 1
        if self.map[west[0]][west[1]] == TILE_TOWER:
            dmg += 1
        if self.map[east[0]][east[1]] == TILE_TOWER:
            dmg += 1
        if self.map[south[0]][south[1]] == TILE_TOWER:
            dmg += 1
        if self.map[position[0]][position[1]] == TILE_SWAMP:
            dmg *= 3
        return dmg

    def heuristic_estimate(self, position):
        # if self.heuristic == 'manhattan':
        #    return abs(position[0] - self.goal_state[0]) + \
        #           abs(position[1] - self.goal_state[1])
        return 0

    def pop_from_frontier(self):
        if self.current_method == 'ucs':
            min_index = []
            min_cost = 10000000
            for iter_index in self.frontier_queue:
                iter_node = self.frontier[iter_index]
                if iter_node.cost < min_cost:
                    min_index = [iter_index]
                    min_cost = iter_node.cost
                elif iter_node.cost == min_cost:
                    min_index.append(iter_index)
            min_x = 10000000
            min_y = 10000000
            for iter_index in min_index:
                if iter_index[0] < min_x:
                    min_x = iter_index[0]
                    min_y = iter_index[1]
                elif iter_index[0] == min_x:
                    if iter_index[1] < min_y:
                        min_y = iter_index[1]
            self.frontier_queue.remove((min_x, min_y))
            return (min_x, min_y)

    def extract_from_frontier(self):
        if self.current_method in ('dfs'):
            index = self.frontier_queue.pop()
            self.nodes_examined += 1
            return [index, self.frontier[index]]
        if self.current_method in ('bfs'):
            index = self.frontier_queue.popleft()
            self.nodes_examined += 1
            return [index, self.frontier[index]]
        if self.current_method in ('ucs', 'greedy'):
            index = self.pop_from_frontier()
            self.nodes_examined += 1
            # print(index)
            return [index, self.frontier[index]]

    def add_to_frontier(self, elements):
        for element in elements:
            index = element[0]
            node = element[1]
            if self.current_method in ('dfs', 'bfs', 'ucs'):
                if self.in_explored_list(index):
                    continue
                elif self.in_frontier(index):
                    if self.frontier[index].cost > node.cost:
                        self.frontier[index] = node
                else:
                    self.frontier[index] = node
                    self.frontier_queue.append(index)

    def add_to_explored_list(self, index, node):
        self.explored_list[index] = node

    def is_traversable(self, index):
        val = self.map[index[0]][index[1]]
        if val == TILE_WALL or val == TILE_TOWER:
            return False
        return True

    def generate_successors(self, index, node):
        children = []
        x = index[0]
        y = index[1]
        north = (x - 1, y)
        west = (x, y - 1)
        east = (x, y + 1)
        south = (x + 1, y)
        # print(index, node)
        if self.current_method in ('dfs', 'bfs', 'ucs', 'greedy'):
            if self.is_traversable(south):
                new_index = south
                new_node = search_node_t(new_index, node,
                                         self.map[new_index[0]][new_index[1]],
                                         self.damage(new_index),
                                         self.heuristic_estimate(new_index))
                children.append((new_index, new_node))
            if self.is_traversable(east):
                new_index = east
                new_node = search_node_t(new_index, node,
                                         self.map[new_index[0]][new_index[1]],
                                         self.damage(new_index),
                                         self.heuristic_estimate(new_index))
                children.append((new_index, new_node))
            if self.is_traversable(west):
                new_index = west
                new_node = search_node_t(new_index, node,
                                         self.map[new_index[0]][new_index[1]],
                                         self.damage(new_index),
                                         self.heuristic_estimate(new_index))
                children.append((new_index, new_node))
            if self.is_traversable(north):
                new_index = north
                new_node = search_node_t(new_index, node,
                                         self.map[new_index[0]][new_index[1]],
                                         self.damage(new_index),
                                         self.heuristic_estimate(new_index))
                children.append((new_index, new_node))
            if self.current_method == 'dfs':
                return children
            if self.current_method in ('bfs', 'ucs', 'greedy'):
                return children[::-1]

    def generate_path(self, index, node):
        iter_node = node
        path = ''
        path_length = 0
        path_cost = node.cost # + cost(node.parent.tile)
        print('Health left', node.health)
        while iter_node.parent != ():
            path_length += 1
            path += direction(iter_node.parent.position, iter_node.position)
            #  path_cost += cost(iter_node.tile)
            iter_node = iter_node.parent
        return path[::-1], path_length, path_cost

    def print_path(self, index, node):
        local_map = copy.deepcopy(self.map)
        iter_node = node
        while iter_node.parent != ():
            if iter_node.tile != TILE_GOAL and iter_node.tile != TILE_START:
                local_map[iter_node.position[0]][iter_node.position[1]] = '+'
            iter_node = iter_node.parent
        for row in local_map:
            print(''.join(row))
        return

    def dfs(self):
        path = []
        path_length = -1
        path_cost = -1
        nodes_examined = -1
        self.current_method = 'dfs'
        print('Running DFS')
        self.init_search()
        while 1:
            if not self.frontier_queue:
                break
            [index, node] = self.extract_from_frontier()
            #  print("index ", index)
            if self.is_goal(index):
                print('At goal')
                nodes_examined = self.nodes_examined
                [path, path_length, path_cost] = \
                    self.generate_path(index, node)
                self.print_path(index, node)
                break
            self.add_to_explored_list(index, node)
            children = self.generate_successors(index, node)
            #  print("Successors", [i[0] for i in children])
            self.add_to_frontier(children)
        return path, path_length, path_cost, nodes_examined

    def bfs(self):
        path = []
        path_length = -1
        path_cost = -1
        nodes_examined = -1
        self.current_method = 'bfs'
        print('Running BFS')
        self.init_search()
        while 1:
            if not self.frontier_queue:
                break
            [index, node] = self.extract_from_frontier()
            #  print("index ", index)
            if self.is_goal(index):
                print('At goal')
                nodes_examined = self.nodes_examined
                [path, path_length, path_cost] = \
                    self.generate_path(index, node)
                self.print_path(index, node)
                break
            self.add_to_explored_list(index, node)
            children = self.generate_successors(index, node)
            #  print("Successors", [i[0] for i in children])
            self.add_to_frontier(children)
        return path, path_length, path_cost, nodes_examined

    def ucs(self):
        path = []
        path_length = -1
        path_cost = -1
        nodes_examined = -1
        self.current_method = 'ucs'
        print('Running UCS')
        self.init_search()
        while 1:
            if not self.frontier_queue:
                break
            [index, node] = self.extract_from_frontier()
            #  print("index ", index)
            if self.is_goal(index):
                print('At goal')
                nodes_examined = self.nodes_examined
                [path, path_length, path_cost] = \
                    self.generate_path(index, node)
                self.print_path(index, node)
                break
            self.add_to_explored_list(index, node)
            children = self.generate_successors(index, node)
            #  print("Successors", [i[0] for i in children])
            self.add_to_frontier(children)
        return path, path_length, path_cost, nodes_examined

    def graph_search(self, search_method):
        result = {
            'Method': '',
            'Path': [],
            'Path Length': -1,
            'Path Cost': -1,
            'Nodes Examined': -1
        }
        print('-'*50)

        [path, path_length, path_cost, nodes_examined] = \
            self.search_method[search_method]()

        result['Method'] = search_method
        result['Path'] = path
        result['Path Cost'] = path_cost
        result['Path Length'] = path_length
        result['Nodes Examined'] = nodes_examined

        for key, value in result.items():
            print(key, value)

        return result

if __name__ == "__main__":
    input_file = process_arguments(sys.argv[1:])
    input_map = generate_map(input_file)
    [start_state, goal_state] = generate_important_states(input_map)
    search_tree = search_tree_t(start_state, goal_state, input_map)
    search_tree.graph_search('dfs')
    search_tree = search_tree_t(start_state, goal_state, input_map)
    search_tree.graph_search('bfs')
    search_tree = search_tree_t(start_state, goal_state, input_map)
    search_tree.graph_search('ucs')

